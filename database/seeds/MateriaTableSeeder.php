<?php

use Illuminate\Database\Seeder;

class MateriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('t_materias')->delete();

    	$materias = [
        	['nombre' => 'matematicas', 'activo' => 1],
        	['nombre' => 'programacion I', 'activo' => 1],
        	['nombre' => 'ingenieria de software', 'activo' => 1]
        ];

        DB::table('t_materias')->insert($materias);
    }
}
