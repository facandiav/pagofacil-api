<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('t_alumnos')->delete();

        DB::table('t_alumnos')->insert([
        	'nombre' => 'John',
        	'ap_paterno' => 'Dow',
        	'ap_materno' => 'Down',
        	'activo' => 1,
        ]);
    }
}
