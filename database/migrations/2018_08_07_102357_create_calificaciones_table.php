<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_calificaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_t_calificaciones');
            $table->unsignedInteger('id_t_alumnos')->index();
            $table->unsignedInteger('id_t_materias')->index();
            $table->decimal('calificacion', 10, 2)->nullable();
            $table->date('fecha_registro')->nullable();
            
        });

        Schema::table('t_calificaciones', function($table){
            $table->engine = 'InnoDB';
            $table->foreign('id_t_alumnos')->references('id_t_alumnos')->on('t_alumnos');
            $table->foreign('id_t_materias')->references('id_t_materias')->on('t_materias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_calificaciones');
    }
}
