<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_materias', function (Blueprint $table) {
            $table->increments('id_t_materias')->nullable(false);
            $table->string('nombre', 80)->nullable();
            $table->boolean('activo')->nullable();
            // Timestamps not required as specified in schema 
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_materias');
    }
}
