# PAGOFACIL API

## Clonar el Repo

## Inicializar
Para empezar debemos crear una base de datos y modificar la variable **DB_DATABASE** del archivo *.env*. Lo siguientes es ejecutar las migraciones y los seeders con los siguientes comandos:

>php artisan migrate

>composer dump-autoload

>php artisan db:seed

Una vez creadas y populadas las tablas podemos proceder a realizar las pruebas.

> php artisan serve

---

## Probando el API

### POST

La llamada POST deberá ser hacia 
> http://**URL**/api/calificaciones

deberá incluir las siguientes variables 

- id\_t\_alumnos
- id\_t\_materias
- calificacion

A continuación una imagen previsualizando la llamada en POSTMAN

![ejemplo de POST]
(https://i.imgur.com/ugjqL17.png)


### GET
La llamada GET deberá ser hacia 
> http://**URL**/api/alumnos/{id}

donde **{id}** es el alumno del que queremos extraer información. Si el alumno existe, se debe ejecutar el request como en la siguiente imagen. 

![ejemplo de GET]
(https://i.imgur.com/pGeTN52.png)

### PUT
Recordemos que Laravel utiliza una abstracción de POST para las llamadas de actualización PUT, por lo que debemos agregar una variable adicional *\_method* indicando que estaremos haciendo una solicitud de tipo PUT.

La llamada **POST** deberá ser hacia 
> http://**URL**/api/calificaciones/{id}

donde **{id}** es el alumno del que queremos actualizar. Esta llamada deberá incluir las siguientes variables:

- id\_t\_alumnos
- id\_t\_materias
- calificacion
- _method => PUT

![ejemplo de PUT]
(https://i.imgur.com/1afmRdU.png)

Si la calificación, la materia y el alumno existen la respuesta será un mensaje en formato JSON indicando que se realizó la actualización de manera exitosa.


### DELETE

Finalmente, para eliminar un registro de calificación se debe hacer una llamada **DELETE** hacia
>http://**URL**/api/calificaciones/{id}

Si la calificación existe, será eliminada y tendremos una respuesta exitosa.
