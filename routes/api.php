<?php

use App\Alumno;
use Illuminate\Http\Request;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\CalificacionController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
/*
Route::get('alumno/{id}', function (Request $request) {
	return Alumno::find($id);
});
*/

Route::get('alumnos/{id}', 'AlumnoController@show');

Route::post('calificaciones', 'CalificacionController@store');

Route::put('calificaciones/{id}', 'CalificacionController@update');

Route::delete('calificaciones/{id}', 'CalificacionController@destroy');