<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    /**
     * Configuration attributes
     *
     * @var var
     */
    protected $table = 't_alumnos';
    protected $primaryKey = 'id_t_alumnos';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'ap_paterno', 'ap_materno', 'activo'];

    /**
     * The attributes that are casted.
     *
     * @var array
     */
    protected $casts = [
    	'activo' => 'boolean'
    ];

    public function calificaciones() {
        return $this->hasMany(Calificacion::class, 'id_t_alumnos');
    } 

    

}
