<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCalificacion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_t_alumnos' => 'required|exists:t_alumnos,id_t_alumnos',
            'id_t_materias' => 'required|exists:t_materias,id_t_materias',
            'calificacion' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id_t_alumnos.required' => 'Alumno is required',
            'id_t_alumnos.exists' => 'Alumno does not exist',
            'id_t_materias.required' => 'Materia is required',
            'id_t_materias.exists' => 'Materia does not exist',
            'calificacion.required' => 'Calificacion is required'
        ];
    }
}
