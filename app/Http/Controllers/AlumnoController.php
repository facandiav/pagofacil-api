<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Materia;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\AlumnoResource;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alumno = Alumno::find($id);
        //return $alumno;
        
        $calificaciones = collect([]);

        foreach ($alumno->calificaciones as $calificacion) {
            $materia = Materia::find($calificacion->id_t_materias);
            $date = Carbon::createFromFormat('Y-m-d', $calificacion->fecha_registro);
            $calificaciones->push([
                'id_t_alumno' => $alumno->id_t_alumnos,
                'nombre' => $alumno->nombre,
                'apellido' => $alumno->ap_paterno,
                'materia' => $materia->nombre,
                'calificacion' => $calificacion->calificacion,
                'fecha_registro' => date('d/m/Y', strtotime($calificacion->fecha_registro))
            ]);
        }
        $calificaciones->push(['promedio' => $alumno->calificaciones->avg('calificacion')])
        
        ;

        return $calificaciones;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
