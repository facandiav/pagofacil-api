<?php

namespace App\Http\Controllers;

use App\Calificacion;
use Illuminate\Http\Request;

class CalificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'id_t_alumnos' => 'required|exists:t_alumnos,id_t_alumnos',
            'id_t_materias' => 'required|exists:t_materias,id_t_materias',
            'calificacion' => 'required',
        ];

        $messages = [
            'id_t_alumnos.required' => 'Alumno is required',
            'id_t_alumnos.exists' => 'Alumno does not exist',
            'id_t_materias.required' => 'Materia is required',
            'id_t_materias.exists' => 'Materia does not exist',
            'calificacion.required' => 'Calificacion is required'
        ];

        $validator = \Validator::make($request->toArray(), $rules, $messages);
        //$validateData = $request->validate();

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        try {
            $calificacion = Calificacion::create([
                'id_t_alumnos' => $request->id_t_alumnos,
                'id_t_materias' => $request->id_t_materias,
                'calificacion' => $request->calificacion,
                'fecha_registro' => date('Y-m-d')
            ]);
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json($exception->errorInfo, 500);
        }   

        return response()->json(['success' => 'ok', 'msg' => 'calificacion registrada'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function show(Calificacion $calificacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Calificacion $calificacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $calificacion = Calificacion::find($id);
        
        if ($calificacion == null) {
            return response()->json(['success' => 'no', 'msg' => 'calificacion no encontrada'], 400);
        }

        $rules = [
            'id_t_alumnos' => 'required|exists:t_alumnos,id_t_alumnos',
            'id_t_materias' => 'required|exists:t_materias,id_t_materias',
            'calificacion' => 'required',
        ];

        $messages = [
            'id_t_alumnos.required' => 'Alumno is required',
            'id_t_alumnos.exists' => 'Alumno does not exist',
            'id_t_materias.required' => 'Materia is required',
            'id_t_materias.exists' => 'Materia does not exist',
            'calificacion.required' => 'Calificacion is required'
        ];

        $validator = \Validator::make($request->toArray(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        try {
            $calificacion->save();
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json($exception->errorInfo, 500);
        }

        $calificacion->update($request->all());

        return response()->json(['success' => 'ok', 'msg' => 'calificacion actualizada'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calificacion  $calificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $calificacion = Calificacion::find($id);
        if ($calificacion == null) {
            return response()->json(['success' => 'no', 'msg' => 'calificacion no encontrada'], 400);
        }
        $calificacion->delete();
        return response()->json(['success' => 'ok', 'msg' => 'calificacion eliminada'], 200);

    }
}
