<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
	protected $table = 't_calificaciones';
    protected $primaryKey = 'id_t_calificaciones';
    public $timestamps = false;

    protected $fillable = ['id_t_alumnos', 'id_t_materias', 'calificacion', 'fecha_registro'];

    public function alumno() {
    	return $this->belongsTo(Alumno::class, 'id_t_alumnos'); 
    }
    
    public function materia() {
    	return $this->belongsTo(Materia::class, 'id_t_materias'); 
    }
}
